local _ = require 'moses'

-- test _.each and _.map
_.each(_.map({1,2,3}, function(x) return x*x end), print)
